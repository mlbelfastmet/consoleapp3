Imports System

Module Program
    Sub Main(args As String())
        Dim userInput As String
        Console.WriteLine("Enter a number:")
        userInput = Console.ReadLine()
        Console.WriteLine("You entered: " & userInput)

        Dim MyRandomNumber = GetRandom(1, 5)

        Try
            If (MyRandomNumber = userInput) Then
                Console.WriteLine("Correct!")
            Else
                Console.WriteLine("Wrong! The right answer was: " & MyRandomNumber)
            End If
        Catch
            Console.WriteLine("Invalid Input!")
        End Try


    End Sub

    Public Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        Dim Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function


End Module
